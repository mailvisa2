(* Common functionality for both mailvisa and mailvisad. *)

open Unix

(** Get a value from a hash table. If the key is not present
    in the hash table, the behavior depends on whether a
    default value is supplied. If it is, the default value
    is returned. If not, Not_found is raised.
*)
let hash_get table key ?default =
  match default with
    | None -> Hashtbl.find table key
    | Some default ->
	if Hashtbl.mem table key
	then Hashtbl.find table key
	else default

(** Get a value from a hash table, converted using the
    given converter function. If the requested key does
    not exist in the hash, return the default value if
    given; else, raise Not_found.
*)
let hash_get_converted ?default table converter key =
  match default with
    | None -> converter (Hashtbl.find table key)
    | Some default ->
	if Hashtbl.mem table key
	then converter (Hashtbl.find table key)
	else default

(** Get a string from a hash table and convert it to an
    int. If the key is not found in the hash table,
    return the default value if given; else, raise
    Not_found.
*)
let hash_get_int ?default table key =
  match default with
    | None ->
	hash_get_converted table int_of_string key
    | Some default ->
	(* hash_get_converted table int_of_string key ~default:default *)
	hash_get_converted table int_of_string key ~default:default

(** Get a string from a hash table and convert it to a
    bool. If the key is not found in the hash table,
    return the default value if given; else, raise
    Not_found.
*)
let hash_get_bool ?default table key =
  match default with
    | None ->
	hash_get_converted table bool_of_string key
    | Some default ->
	hash_get_converted table bool_of_string key ~default:default

(** Get a string from a hash table and convert it to a
    float. If the key is not found in the hash table,
    return the default value if given; else, raise
    Not_found.
*)
let hash_get_float ?default table key =
  match default with
    | None ->
	hash_get_converted table float_of_string key
    | Some default ->
	hash_get_converted table float_of_string key ~default:default

(** Look up a key in a hash table. If it exists, return
    (Some value), where value is the value associated
    with the key. If it doesn't exist, return None.
*)
let hash_get_maybe table key =
  if Hashtbl.mem table key
  then Some (Hashtbl.find table key)
  else None

(** Get a value from a hash table, using the given default if
    the given key does not exist in the hash table. *)
let get_option_with_default options option default =
  if Hashtbl.mem options option
  then Hashtbl.find options option
  else default

(** Increment an integer reference. *)
let increment n = n := !n + 1

(** Iterate over the lines in an input channel. *)
let rec iter_lines (func : string -> unit) channel =
  try
    let line = input_line channel in
      func line;
      iter_lines func channel
  with End_of_file -> ()

(** Read everything from an input channel and append it to the given
    Buffer.
*)
let slurp_into_buffer ?(blocksize=4096) channel buffer =
  let bytes = String.create blocksize in
  let rec aux _ =
    let bytes_read = input channel bytes 0 blocksize in
      if bytes_read > 0 then begin
	Buffer.add_substring buffer bytes 0 bytes_read;
	aux ()
      end
  in aux ()

(** Read everything from an input channel and return it as a string. *)
let slurp ?(blocksize=4096) channel =
  let buffer = Buffer.create blocksize in
    slurp_into_buffer channel buffer ~blocksize:blocksize;
    Buffer.contents buffer

(** Connect to a UNIX socket. *)
let unix_connect name =
  let conn = socket PF_UNIX SOCK_STREAM 0 in
    connect conn (ADDR_UNIX name);
    conn
