(* Add the words from the given messages to the given wordlist *)

open Common
open Tokenize
open Wordlist

(* Defaults *)

let default_confdir = (Unix.getenv "HOME") ^ "/settings/mailvisa2"
let default_ignore_spam_headers = true
let default_max_word_length = 40
let default_weed_threshold = 1.0
let weed_count = ref 100000

let usage = "USAGE: mailvisa-add-messages [options] <wordlist> [<message> ...]"
let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-i              Include X-Spam headers in analysis
-w <num>	Weed wordlist every <num> words (default: 10000).
                Use -w 0 to disable weeding
-t <num>        Weed words that occur <num> or fewer times (default: 1)"

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 2 in
  let i = ref 1 in
  let add_option name = increment i; Hashtbl.add options name args.(!i) in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-i" -> Hashtbl.add options "ignore_spam_headers" "false"
	 | "-c" -> add_option "confdir"
	 | option when (String.get option 0) = '-' ->
	       output_string stderr ("Invalid option: " ^ option ^ "\n");
	       exit 0x80
	 | filename ->
	     if Hashtbl.mem options "filename" then
	       Hashtbl.add options "messages" filename
	     else
	       Hashtbl.add options "filename" filename
      );
      increment i
    done;
    options

(* Initialization *)

let options = parse_options Sys.argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

(* Check command line arguments *)
let _ =
  if (hash_get_maybe options "filename") = None then begin
    prerr_endline "No score file name specified";
    prerr_endline usage;
    exit 0x80
  end

(* Set values from command line options *)
let filename = absolute_path (Hashtbl.find options "filename")
let ignore_spam_headers = hash_get_bool options "ignore_spam_headers" ~default:default_ignore_spam_headers
let max_word_length = default_max_word_length
let messages = List.rev (Hashtbl.find_all options "messages")
let weed_threshold = default_weed_threshold

(* Functions *)

(** Weed word list. Any words that occur in the list fewer than weed_threshold
    times are removed.
*)
let weed words =
  prerr_endline "Weeding...";
  let count = Hashtbl.length words in
    Hashtbl.iter (fun word count ->
		    if count <= weed_threshold then Hashtbl.remove words word)
      words;
    let weeded = count - (Hashtbl.length words) in
      prerr_endline ((string_of_int weeded) ^ " words discarded");
      if weeded < (!weed_count / 10) then
	weed_count := !weed_count * 2

(** Read words from channel and add them to words. *)
let add_words_from_channel words channel =
  iter_lines (fun line ->
		if ((not ignore_spam_headers) ||
		      ((String.length line) < 6) ||
		      (not ("X-Spam" = (String.sub line 0 6)))) then begin
		  List.iter (fun word ->
			       if (String.length word) <= max_word_length then begin
				 (match (hash_get_maybe words word) with
				    | None -> Hashtbl.add words word 1.0
				    | Some n -> Hashtbl.replace words word (n +. 1.0));
				 
				 if (!weed_count > 0) && ((Hashtbl.length words) mod !weed_count = 0) then
				       weed words
			       end
			    ) (tokenize line)
		end
	     ) channel

let add_words_from_file words filename =
  let channel = open_in filename in
    add_words_from_channel words channel;
    close_in channel

let _ =
  (* Get message count and wordlist from named file,
     or initialize to empty values if the file does
     not exist. *)
  let message_count, words =
    try
      let wordlist = load_wordlist filename in
	(ref (wordlist_messages wordlist), wordlist_words wordlist)
    with Sys_error _ ->
      prerr_endline (filename ^ " not found, will create new file");
      (ref 0, Hashtbl.create !weed_count)
  in
    List.iter (fun message ->
		 prerr_endline ("Adding " ^ message);
		 add_words_from_file words message;
		 increment message_count;
	      ) messages;
    prerr_string ("Writing " ^ filename ^ "...");
    save_wordlist (create_wordlist !message_count words) filename;
    prerr_endline "done"
