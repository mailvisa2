(* Remove the words from the given messages from the given wordlist *)

open Common
open Tokenize
open Wordlist

(* Defaults *)

let default_confdir = (Unix.getenv "HOME") ^ "/settings/mailvisa2"
let default_ignore_spam_headers = true
let default_max_word_length = 40

let usage = "USAGE: mailvisa-remove-messages [options] <wordlist> [<message> ...]"
let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-i              Include X-Spam headers in analysis"

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 2 in
  let i = ref 1 in
  let add_option name = increment i; Hashtbl.add options name args.(!i) in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-i" -> Hashtbl.add options "ignore_spam_headers" "false"
	 | "-c" -> add_option "confdir"
	 | option when (String.get option 0) = '-' ->
	       output_string stderr ("Invalid option: " ^ option ^ "\n");
	       exit 0x80
	 | filename ->
	     if Hashtbl.mem options "filename" then
	       Hashtbl.add options "messages" filename
	     else
	       Hashtbl.add options "filename" filename
      );
      increment i
    done;
    options

(* Initialization *)

let options = parse_options Sys.argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

(* Check command line arguments *)
let _ =
  if (hash_get_maybe options "filename") = None then begin
    prerr_endline "No score file name specified";
    prerr_endline usage;
    exit 0x80
  end

(* Set values from command line options *)
let filename = absolute_path (Hashtbl.find options "filename")
let ignore_spam_headers = hash_get_bool options "ignore_spam_headers" ~default:default_ignore_spam_headers
let max_word_length = default_max_word_length
let messages = Hashtbl.find_all options "messages"

(* Functions *)

(** Read words from channel and remove them from words. *)
let remove_words_from_channel words channel =
  iter_lines (fun line ->
		if ((not ignore_spam_headers) ||
		      ((String.length line) < 6) ||
		      (not ("X-Spam" = (String.sub line 0 6)))) then begin
		  List.iter (fun word ->
			       if (String.length word) <= max_word_length then
				 match (hash_get_maybe words word) with
				   | None -> ()
				   | Some n when n <= 1.0 -> Hashtbl.remove words word
				   | Some n -> Hashtbl.replace words word (n -. 1.0)
			    ) (tokenize line)
		end
	     ) channel

let remove_words_from_file words filename =
  let channel = open_in filename in
    remove_words_from_channel words channel;
    close_in channel

let _ =
  (* Get message count and wordlist from named file *)
  let message_count, words =
    try
      let wordlist = load_wordlist filename in
	(ref (wordlist_messages wordlist), wordlist_words wordlist)
    with Sys_error _ ->
      prerr_endline ("Cannot load wordlist from " ^ filename ^ ", aborting");
      exit 1
  in
    List.iter (fun message ->
		 prerr_endline ("Removing " ^ message);
		 remove_words_from_file words message;
		 message_count := (if !message_count > 0
		                   then (!message_count - 1)
		                   else 0)
	      ) messages;
    prerr_string ("Writing " ^ filename ^ "...");
    save_wordlist (create_wordlist !message_count words) filename;
    prerr_endline "done"
