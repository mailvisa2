(* Functions for logging. *)

(* Uses Printf and Unix. *)

open Printf

(* Loglevels. *)
let loglevel_fatal = 100
let loglevel_error = 200
let loglevel_warning = 300
let loglevel_notice = 400
let loglevel_debug = 500

(** Channel to log to. Default is stderr. *)
let log_channel = ref stderr

(** Loglevel threshold. *)
let loglevel = ref loglevel_notice

(** Log a raw string. *)
let log_raw string =
  let channel = !log_channel in
    output_string channel string;
    flush channel

(** Convert a timestamp (float) to a string for use in logging. *)
let string_of_timestamp time =
  let tm = Unix.gmtime time in
  let seconds = (float_of_int tm.Unix.tm_sec) +. (fst (modf time)) in
    sprintf "%04d-%02d-%02d %02d:%02d:%02.3f"
      (tm.Unix.tm_year + 1900) (tm.Unix.tm_mon + 1) tm.Unix.tm_mday
      tm.Unix.tm_hour tm.Unix.tm_min seconds

(** Log a string with a timestamp. *)
let log_with_timestamp time string =
  log_raw ((string_of_timestamp time) ^ " " ^ string)

(** Log a message with the given level, if the level is
    urgent enough.
*)
let log level message =
  if level <= !loglevel then
    let time = Unix.gettimeofday () in
      log_with_timestamp time (message ^ "\n")

let log_fatal message = log loglevel_fatal ("FATAL " ^ message)
let log_error message = log loglevel_error ("ERROR " ^ message)
let log_warning message = log loglevel_warning ("WARNING " ^ message)
let log_notice message = log loglevel_notice ("NOTICE " ^ message)
let log_debug message = log loglevel_debug ("DEBUG " ^ message)

let set_log_channel channel = log_channel := channel
let set_loglevel level = loglevel := level
let get_loglevel _ = !loglevel
