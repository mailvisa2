(* Daemon for Mailvisa 2. This is where the actual spam scoring happens. *)

open Sys
open Unix

open Common
open Logger
open Score
open Wordlist

(* Defaults *)
let default_confdir = (Sys.getenv "HOME") ^ "/settings/mailvisa2"
let default_sockpath = "mailvisad.sock"
let default_scorefile = "scores"
let default_pidfile = "mailvisad.pid"
let default_logfile = "mailvisad.log"

let usage = "USAGE: mailvisad [options]"
let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa2)
-f <path>	Load scores from <path> (default: scores)
-l <path>	Log to <path> (default: mailvisad.log)
-p <path>	Use <path> as pidfile (default: mailvisad.pid)
-s <path>	Use <path> as socket (default: mailvisad.sock)"

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 8 in
  let i = ref 1 in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-c" ->
	     increment i;
	     Hashtbl.add options "confdir" args.(!i)
	 | "-s" ->
	     increment i;
	     Hashtbl.add options "sockpath" args.(!i)
	 | "-f" ->
	     increment i;
	     Hashtbl.add options "scorefile" args.(!i)
	 | "-l" ->
	     increment i;
	     Hashtbl.add options "logfile" args.(!i)
	 | "-p" ->
	     increment i;
	     Hashtbl.add options "pidfile" args.(!i)
	 | option ->
	     output_string Pervasives.stderr ("Invalid option: " ^ option ^ "\n");
	     exit 0x80);
      increment i
    done;
    options

(** Receive up to maxlen bytes from a socket. *)
let sock_recv sock maxlen =
  let str = String.create maxlen in
  let n = recv sock str 0 maxlen [] in
    String.sub str 0 n

(** Send a string over a socket. *)
let sock_send sock str =
  ignore (send sock str 0 (String.length str) [])

(** Unlink a path if it is a socket. *)
let unlink_if_socket path =
  try
    let stats = lstat path in
      if stats.st_kind = S_SOCK then unlink path
  with Unix_error _ ->
    ()

(* Parse command line and set configuration. *)
let options = parse_options Sys.argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

let sockpath = absolute_path (get_option_with_default options "sockpath" default_sockpath)
let scorefile = absolute_path (get_option_with_default options "scorefile" default_scorefile)
let pidfile = absolute_path (get_option_with_default options "pidfile" default_pidfile)
let logfile = absolute_path (get_option_with_default options "logfile" default_logfile)

let _ =
  (* Open logfile. *)
  set_log_channel (open_out_gen [Open_wronly; Open_append; Open_creat; Open_text] 0o600 logfile);

  (* Set loglevel. *)
  set_loglevel loglevel_notice;

  (* Wrap the whole rest of the program with exception handlers that
   * log errors. *)
  Printexc.record_backtrace true;
  try
    log_notice "Starting up";

    (* Open socket. *)
    let sock = socket PF_UNIX SOCK_STREAM 0 in
    let sockaddr = (ADDR_UNIX sockpath) in
    let _ = 
      (try
	 log_debug "Creating socket";
	 bind sock sockaddr
       with Unix_error(code,_,_) ->
	 if code = EADDRINUSE then begin
	   log_debug "Address already in use";
	   (* Address already in use - test if socket is really active. *)
	   (* If we can connect to the address, assume mailvisad is already running. *)
	   try
	     let conn = socket PF_UNIX SOCK_STREAM 0 in
	       connect conn sockaddr;
	       close conn;
	       let msg = ("mailvisad already bound to " ^ sockpath ^ ", exiting\n")
	       in
		 log_notice msg;
		 prerr_endline msg;
		 exit 0
	   with Unix_error _ ->
	     (* Couldn't connect. If sockpath is a socket, remove it and try binding again. *)
	     unlink_if_socket sockpath;
	     log_debug "Removed socket file, trying to bind again";
	     try bind sock sockaddr
	     with Unix_error (code,_,_) ->
	       log_fatal (error_message code);
	       exit 1
	 end);
      log_debug "Successfully created socket";
      listen sock 3;

      (* Fork and exit parent. *)
      if (fork ()) != 0 then exit 0;

      (* Set at_exit handler to close and remove socket and pidfile. *)
      at_exit (fun _ ->
		 log_notice "Exiting";
		 close sock;
		 unlink_if_socket sockpath;
		 unlink pidfile);

      (* Install SIGTERM handler to gracefully exit program. *)
      set_signal sigterm (Signal_handle (fun _ ->
					   log_notice "Got SIGTERM";
					   exit 0));

      (* Install SIGCHLD handler to collect exited child processes. *)
      set_signal sigchld (Signal_handle (fun _ ->
					   (* Keep calling waitpid until there are no more
					      processes to collect. *)
					   let rec collect _ =
					     try
					       let pid, _ = waitpid [WNOHANG] (-1) in
						 if pid != -1 then collect ()
					     with _ -> ()
					   in collect ()));

      (* Detach process from terminal. *)
      close stdin;
      ignore (setsid ());
      
      (* Write pid file. *)
      let pid_channel = open_out pidfile in
	output_string pid_channel ((string_of_int (getpid ())) ^ "\n");
	close_out pid_channel
    in

    (* Load scores. *)
    log_debug "Loading scores";
    let scores = load_wordlist scorefile in
      log_debug "Scores loaded";
      (* Main loop.
	 Accept a connection on the socket, read the first so many bytes of the message,
	 extract the most interesting words, compute spam probability, and
	 send result out over the connection.
      *)
      while true do
	log_debug "Ready for new connection";
	let connection, address = accept sock in
	  log_debug "Received new connection";
	  (* Handle connection in new child process. *)
	  try 
	    (*
	      let pid = fork () in
	      if pid = 0 then
	    (* Child process; handle connection. *)
	    *)
	    begin
	      (try
		 let message = sock_recv connection 16384 in
		   if (String.length message) == 0 then
		     (* Empty message; ignore. *)
		     log_debug "Ignoring empty message"
		   else begin
		     let probability = spam_probability (extract_interesting_words message scores) scores in
		     let prob_str = string_of_float probability in
		       log_debug ("Spam probability: " ^ prob_str);
		       sock_send connection prob_str;
		   end;
		   log_debug "Closing connection";
		   close connection
	       with exn ->
		 log_error ((Printexc.to_string exn) ^
			      "\n" ^
			      (Printexc.get_backtrace ()));
		 close connection;
		 raise exn);
	      (* exit 0; *)
	    end
	      (*
		else
	      (* Parent process; close connection. *)
		close connection
	      *)
	  with Unix_error (code,_,_) ->
	    log_error (error_message code)
      done
  with
    | exn ->
	log_fatal ((Printexc.to_string exn) ^
		     "\n" ^
		     (Printexc.get_backtrace ()));
	exit 1
