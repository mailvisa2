(* Read scores file and display the spam scores of each word. *)

open Common
open Wordlist

(* Defaults *)

let default_confdir = (Unix.getenv "HOME") ^ "/settings/mailvisa2"
let default_scorefile = "scores"

let usage = "USAGE: mailvisa-view-scores [options]"
let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-f <path>	Load scores from <path> (default: scores)"

(* Functions *)

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 2 in
  let i = ref 1 in
  let add_option name = increment i; Hashtbl.add options name args.(!i) in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-c" -> add_option "confdir"
	 | "-f" -> add_option "scorefile"
	 | option ->
	     output_string stderr ("Invalid option: " ^ option ^ "\n");
	     exit 0x80);
      increment i
    done;
    options

(* Initialization *)

let options = parse_options Sys.argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

(* Set constants from command line options *)
let scorefile = absolute_path (hash_get options "scorefile" ~default:default_scorefile)

(* Display scores *)
let _ =
  let wordlist = load_wordlist scorefile in
    Hashtbl.iter
      (fun word score -> print_endline (word ^ ": " ^ (string_of_float score)))
      (wordlist_words wordlist)
