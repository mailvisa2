(** Filter tokens by length. *)
let filter_tokens tokens =
    List.filter (fun token -> String.length token >= 1) tokens

(** Split a string into tokens according to the rules used by Mailvisa 1 *)
let tokenize_version_1 str =
  filter_tokens (Str.split (Str.regexp "[^0-9A-Za-z_]") str)

(** Split a string into tokens. This function considers a token to be
    a sequence of one or more token characters, where token characters
    are any character that isn't a control character (\x00 through \0x1f)
    or a punctuaction character (space, and the characters in the string
    "!\"'().:;<>?[]`{}").
*)
let tokenize_version_2 str =
  filter_tokens (Str.split (Str.regexp "[][\x00-\x20!\"'().:;<>?`{}]") str)

(** Split a string into tokens *)
let tokenize = tokenize_version_2
