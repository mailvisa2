type wordlist

val load_wordlist_from_channel : in_channel -> wordlist
val save_wordlist_to_channel : wordlist -> out_channel -> unit
val get_incidence : wordlist -> string -> float
val get_score_with_default : wordlist -> string -> float -> float
val get_score : wordlist -> string -> float
val load_wordlist : string -> wordlist
val save_wordlist : wordlist -> string -> unit
val set_score : wordlist -> string -> float -> unit
val create_wordlist : int -> (string, float) Hashtbl.t -> wordlist
val wordlist_messages : wordlist -> int
val wordlist_words : wordlist -> (string, float) Hashtbl.t
