(* Calculate words' spam probability based on frequency in
   spam and ham messages.
*)

open Common
open Wordlist

(* Defaults *)

let default_confdir = (Sys.getenv "HOME") ^ "/settings/mailvisa2"
let default_scorefile = "scores"
let default_goodfile = "good"
let default_badfile = "bad"
let default_good_multiplier = 1.0

let usage = "USAGE: mailvisa-calculate-scores [options]"

let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa2)
-g <file>	Load good words from <file> (default: \"good\")
-b <file>	Load bad words from <file> (default: \"bad\")
-f <file>	Write scores to <file> (default: \"scores\")
-m <num>	Multiply number of good occurrences by <num> (default: 1.0)"

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 8 in
  let i = ref 1 in
  let add_flag name = Hashtbl.add options name "true" in
  let add_option name = increment i; Hashtbl.add options name args.(!i) in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-b" -> add_option "badfile"
	 | "-c" -> add_option "confdir"
	 | "-f" -> add_flag "scorefile"
	 | "-g" -> add_option "goodfile"
	 | "-m" -> add_option "good_multiplier"
	 | option ->
	     output_string Pervasives.stderr ("Invalid option: " ^ option ^ "\n");
	     exit 0x80);
      increment i
    done;
    options

(* Initialization *)

let options = parse_options Sys.argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

(* Set constants from command line options *)
let badfile = absolute_path (hash_get options "badfile" ~default:default_badfile)
let scorefile = absolute_path (hash_get options "scorefile" ~default:default_scorefile)
let goodfile = absolute_path (hash_get options "goodfile" ~default:default_goodfile)
let good_multiplier = hash_get_float options "good_multiplier" ~default:default_good_multiplier

(* Functions *)

(** Calculate the score of a word *)
let calculate_word_score word good bad =
  let b = get_incidence bad word in
  let g = good_multiplier *. (get_incidence good word) in
  let score = b /. (b +. g) in
    if score < 0.1 then
      0.1
    else if score > 0.9 then
      0.9
    else
      score

let _ =
  prerr_string ("Loading good words from " ^ goodfile ^ "...");
  flush stderr;
  let good = load_wordlist goodfile in
    prerr_endline ((string_of_int (Hashtbl.length (wordlist_words good))) ^ " words loaded");
    prerr_string ("Loading bad words from " ^ badfile ^ "...");
    flush stderr;
    let bad = load_wordlist badfile in
      prerr_endline ((string_of_int (Hashtbl.length (wordlist_words bad))) ^ " words loaded");
      prerr_string "Calculating probabilities...";
      flush stderr;
      let score = Hashtbl.create 10000 in

	Hashtbl.iter (fun word count ->
			if (not (Hashtbl.mem score word)) && count > 4.0 then
			  Hashtbl.add score word (calculate_word_score word good bad))
	  (wordlist_words bad);

	Hashtbl.iter (fun word count ->
			if (not (Hashtbl.mem score word)) && count > 4.0 then
			  Hashtbl.add score word (calculate_word_score word good bad))
	  (wordlist_words good);
	prerr_endline "done";

	let scores = create_wordlist ((wordlist_messages bad) + (wordlist_messages good)) score in
	  save_wordlist scores scorefile
