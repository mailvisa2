(* Functions to compute the spam score of messages. *)

open Tokenize
open Wordlist

(* Data structure to keep track of interesting words. *)
type interesting_words = {
  mutable lowest_extremity : float;
  mutable length : int;
  mutable words : string list;
}

(** Create a new, empty interesting_words record. *)
let make_interesting_words _ = { lowest_extremity = 1.0;
				 length = 0;
				 words = [] }

(** Compute the extremity of a word. *)
let extremity word wordlist = abs_float (0.5 -. (get_score wordlist word))

(** Add a word to a list of interesting words, sorted by increasing extremity. *)
let add_word_sorted word words scores =
  List.merge (fun wordx wordy ->
		let relative = (extremity wordx scores) -. (extremity wordy scores) in
		  if relative < 0.0 then -1
		  else if relative > 0.0 then 1
		  else 0)
    words [word]

(** Add a word to a list of interesting words, if it's interesting enough. *)
let add_interesting_word_with_maximum word interesting_words scores max_words =
  let extremity = extremity word scores in
    if (extremity > interesting_words.lowest_extremity) || (interesting_words.length < max_words) then begin
      (* Add the new word, keeping the list of words sorted so the least interesting word comes first. *)
      interesting_words.words <- add_word_sorted word interesting_words.words scores;
      (* Update the lowest extremity as necessary. *)
      if extremity < interesting_words.lowest_extremity then interesting_words.lowest_extremity <- extremity;
      (* If there where fewer than max_words words in the list, increment the word count,
	 otherwise, discard the least interesting word. *)
      if interesting_words.length < max_words then interesting_words.length <- interesting_words.length + 1
      else interesting_words.words <- List.tl interesting_words.words
    end

(** Add a word to a list of interesting words, if it's interesting enough. *)
let add_interesting_word word interesting_words scores = add_interesting_word_with_maximum word interesting_words scores 20

(** Extract the most interesting words from a message. *)
let extract_interesting_words message scores =
  let words = tokenize message in
  let interesting_words = make_interesting_words () in
    List.iter (fun word -> add_interesting_word word interesting_words scores) words;
    interesting_words.words

(** Given a list of interesting words, compute spam probability. *)
let spam_probability words scores =
  let word_scores = List.map (get_score scores) words in
  let prod = List.fold_left ( *.) 1.0 word_scores in
  let div = List.fold_left (fun div x -> div *. (1.0 -. x)) 1.0 word_scores in
    prod /. (prod +. div)
