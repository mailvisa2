(* Mailvisa ispatcher binary *)

open Unix

let usage = "USAGE mailvisa <command> [options]"
let help = "Valid commands are:

add		Add messages to database
calculate	Recalculate scores
check		Check whether a message is spam
remove		Remove messages from database
start		Start daemon
view		View scores

Use mailvisa <command> -h to list options for that command"

(** Execs the program designated by path. The program is
    passed the concatenation of path and args as its arguments.
    I.e. the program name is passed as the zeroth argument, as
    is conventional in Unix.
*)
let exec_program path args =
  execvp path (Array.append [| path |] args)

(** Execs a mailvisa program with the given arguments. If
    callerpath is an explicit path, its dirname is prefixed
    to program. Otherwise, program is sought it the
    system path.
*)
let exec_mailvisa_program callername program args =
  try
    if (Filename.dirname callername) = "." && (Filename.is_implicit callername) then
      exec_program program args
    else
      exec_program (Filename.concat (Filename.dirname callername) program) args
  with error ->
    (match error with
       | Unix_error(errno, _, _) ->
	   prerr_endline ("Error running program " ^ program ^ ": " ^ (error_message errno))
       | _ ->
	   prerr_endline ("Error running program " ^ program));
    exit 1

let _ =
  (* Abort and print usage message if no command was given *)
  if (Array.length Sys.argv) < 2 then begin
    prerr_endline usage;
    exit 0x80
  end;
  (* Extract command and arguments *)
  let progname = Sys.argv.(0) in
  let command = Sys.argv.(1) in
  let args = (let length = Array.length Sys.argv in
		if length > 2
		then Array.sub Sys.argv 2 (length - 2)
		else [||])
  in
    match command with
      | "add" -> exec_mailvisa_program progname "mailvisa-add-messages" args
      | "calculate" -> exec_mailvisa_program progname "mailvisa-calculate-scores" args
      | "check" -> exec_mailvisa_program progname "mailvisa-check" args
      | "help" -> print_endline help
      | "remove" -> exec_mailvisa_program progname "mailvisa-remove-messages" args
      | "start" -> exec_mailvisa_program progname "mailvisad" args
      | "view" -> exec_mailvisa_program progname "mailvisa-view-scores" args
      | _ ->
	  prerr_endline usage;
	  prerr_endline help;
	  exit 0x80
