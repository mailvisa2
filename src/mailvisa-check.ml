(* mailvisa-check - Read a message, send it to mailvisad, and judge if it's ham or spam *)

open Sys
open Unix

open Common

(* Defaults *)

let default_confdir = (getenv "HOME") ^ "/settings/mailvisa2"
let default_sockpath = "mailvisad.sock"
let default_blocksize = 16384
let default_threshold = 0.5
let default_passthrough = true	(** output message *)
let default_header = true	(** output X-Spam headers *)
let default_errorcodes = false	(** suppress error codes if true *)
let default_input = Pervasives.stdin
let default_output = Pervasives.stdout

(* Constants *)

let endl = "\x0d\x0a"
let spam_status = 160

let usage = "USAGE: mailvisa [options]"
let help = "Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-q		Do not output message or X-Spam headers
-e		Do not use exit status to indicate spam
-b <num>	Read <num> bytes at a time (default: 16384)
-t <num>	Threshold for flagging messages as spam (default: 0.5)
-m <command>	Pipe output to <command>
-s <path>	Use <path> to connect to mailvisad (default: mailvisad.sock)"

(* Functions *)

(** Parse command options from args into hash table. *)
let parse_options args =
  let options = Hashtbl.create 8 in
  let i = ref 1 in
  let add_flag name = Hashtbl.add options name "true" in
  let add_option name = increment i; Hashtbl.add options name args.(!i) in
    while !i < (Array.length args) do
      (match args.(!i) with
	 | "-h" ->
	     print_endline usage;
	     print_endline help;
	     exit 0
	 | "-b" -> add_option "blocksize"
	 | "-c" -> add_option "confdir"
	 | "-e" -> add_flag "errorcodes"
	 | "-m" -> add_option "mda"
	 | "-q" -> add_flag "quiet"
	 | "-t" -> add_option "threshold"
	 | "-s" -> add_option "sockpath"
	 | option ->
	     output_string Pervasives.stderr ("Invalid option: " ^ option ^ "\n");
	     exit 0x80);
      increment i
    done;
    options

(* Initialization *)

let options = parse_options argv
let confdir = get_option_with_default options "confdir" default_confdir

(** Create an absolute path from a path, by prepending confdir if the path does not contain a slash. *)
let absolute_path path =
  if String.contains path '/' then path
  else (confdir ^ "/" ^ path)

(* Set constants from command line options *)
let sockpath = absolute_path (hash_get options "sockpath" ~default:default_sockpath)
let blocksize = hash_get_int options "blocksize" ~default:default_blocksize
let threshold = hash_get_float options "threshold" ~default:default_threshold
let quiet = hash_get_bool options "quiet" ~default:false
let passthrough = if quiet then false else default_passthrough
let header = if quiet then false else default_header
let errorcodes = hash_get_bool options "errorcodes" ~default:default_errorcodes
let input_channel = default_input

let output_channel =
  (match hash_get_maybe options "mda" with
     | None -> default_output
     | Some command -> open_process_out command)

(* Main program *)
let _ = 
  let status = ref 0 in
  let msg = String.create blocksize in
  let bytes_read = input input_channel msg 0 blocksize in
    (* Analyze message *)
    begin
      try
	  (* If we didn't get any bytes, raise End_of_file *)
	  if bytes_read = 0 then raise End_of_file;
	  (* We read n bytes; send them on to the daemon. *)
	  let conn =
	    try 
	      unix_connect sockpath
	    with _ ->
	      failwith ("Cannot connect to " ^ sockpath)
	  in
	  let bytes_written = write conn msg 0 bytes_read in
	    ignore bytes_written;
	    shutdown conn SHUTDOWN_SEND;
	    (* Read response from daemon. *)
	    let response = input_line (in_channel_of_descr conn) in
	    let score = (float_of_string response) in
	      if score > threshold then status := spam_status
      with error ->
	status := 1;
	match error with
	  | Failure msg ->
	      prerr_endline msg
	  | Unix_error (errno, funname, _) ->
	      prerr_endline (funname ^ ": " ^ (error_message errno))
	  | _ ->
	      prerr_endline (Printexc.to_string error)
    end;

    (* Output X-Spam: header if requested *)
    if header && (!status = 0 || !status = spam_status) then
      output_string output_channel ("X-Spam: " ^
				      (if !status = spam_status
				       then "true"
				       else "false") ^
				      endl);

    (* Pass through message if requested *)
    if passthrough then begin
      let rec aux n =
	if n > 0 then begin
	  output output_channel msg 0 n;
	  aux (input input_channel msg 0 blocksize)
	end
      in aux bytes_read
    end;

    (* Return status *)
    exit (if errorcodes then 0 else !status)
