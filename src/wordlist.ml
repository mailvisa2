(* Functions for operating on word lists *)

open Str

open Common

type wordlist = {
  messages : int;
  words : (string, float) Hashtbl.t;
}

(** Read headers from an input channel. *)
let read_headers channel =
  let rec aux channel headers =
    let line = input_line channel in
      if (String.length line) = 0 then headers
      else begin
	(match (bounded_split (regexp_string ": ") line 2) with
	   | [key; value] -> Hashtbl.add headers key value
	   | _ -> failwith ("Invalid line in input: " ^ line));
	aux channel headers
      end
  in
    aux channel (Hashtbl.create 4)

(** Read words and scores from an input channel. *)
let read_words_version_1 channel =
  let rec aux channel words = 
    let eof =
      (try
	 let line = input_line channel in
	   (match (bounded_split (regexp_string "\t") line 2) with
	      | [word; score] -> (try
				    Hashtbl.replace words word (float_of_string score)
				  with Failure _ ->
				    prerr_endline ("Cannot convert " ^ score ^ " to float"))
	      | _ -> failwith ("Invalid line in input: " ^ line));
	   false
       with End_of_file -> true) in
      if eof then words else (aux channel words)
  in
    aux channel (Hashtbl.create 0x1000)

(** Load a version 1 wordlist from a channel,
    after headers have been read.
*)
let load_version_1_wordlist_from_channel channel headers =
  { messages = (match hash_get_maybe headers "Messages" with
		  | None -> 0
		  | Some n -> (int_of_string n));
    words = read_words_version_1 channel }

(** Load a wordlist from an input channel. *)
let load_wordlist_from_channel channel =
  let headers = read_headers channel in
    (* Check the word list file format version. *)
    (match hash_get_maybe headers "Version" with
       | None -> (* assume version 1 *)
	   load_version_1_wordlist_from_channel channel headers
       | Some "1" -> load_version_1_wordlist_from_channel channel headers
       | Some version -> failwith ("Cannot handle word lists with version " ^ version))

(** Output a line to an output channel. *)
let output_line channel line =
  output_string channel line;
  output_char channel '\n'

(** Convert a float to a string in Mailvisa syntax. *)
let float_to_string x =
  let str = (string_of_float x) in
    if (String.get str ((String.length str) - 1)) = '.' then str ^ "0" else str

(** Save a version 1 wordlist to an output channel. *)
let save_version_1_wordlist_to_channel wordlist channel =
  (* Write headers. *)
  output_line channel "Content-Type: text/x-mailvisa-wordlist";
  output_line channel "Version: 1";
  output_string channel "Messages: ";
  output_line channel (string_of_int wordlist.messages);
  output_line channel "";
  (* Write words. *)
  Hashtbl.iter (fun word score ->
		  output_string channel word;
		  output_char channel '\t';
		  output_line channel (float_to_string score))
    wordlist.words

(** Save a wordlist to an output channel. *)
let save_wordlist_to_channel wordlist channel =
  save_version_1_wordlist_to_channel wordlist channel

(** Get the incidence of a word *)
let get_incidence wordlist word =
  match hash_get_maybe wordlist.words word with
    | None -> 0.0
    | Some n -> n /. (float_of_int wordlist.messages)

(** Get the score of a word, using the given value as a default. *)
let get_score_with_default wordlist word default =
  match hash_get_maybe wordlist.words word with
    | None -> default
    | Some value -> value

(** Get the score of a word, returning a default value if the score is unknown. *)
let get_score wordlist word = get_score_with_default wordlist word 0.4

(** Load a wordlist from a file. *)
let load_wordlist filename =
  load_wordlist_from_channel (open_in filename)

(** Save a wordlist to a file. *)
let save_wordlist wordlist filename =
  save_wordlist_to_channel wordlist (open_out filename)

(** Set the score for a word in the wordlist (the word is added if it is not present yet). *)
let set_score wordlist word score =
  Hashtbl.replace wordlist.words word score

let create_wordlist messages words =
  { words = words;
    messages = messages }
let wordlist_messages wordlist = wordlist.messages
let wordlist_words wordlist = wordlist.words
